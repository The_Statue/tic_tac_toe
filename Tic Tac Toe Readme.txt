Compile the project in visual studio and then run either through the folder or visual studio.

Main menu:
Play, starts a new game

Continue, will continue an existing game (only the current game, reseting or closing will get rid of the current game)

Exit, closes the application


Main gameplay:

While in game, the first player is able to select a move in 3D space using the 2D representation on the right side of the screen, each 2D tic tac toe board being a representation of one layer of the 3D cube.

When a player clicks a box on the 2D board, it will be filled in along with its 3D counterpart, signaling the next players turn.

Players will repeat this until 3 in a row of one colour is achieved by either side, that player will recieve one point, the game continues until one side recieves 3 points / 3x 3 in a row.
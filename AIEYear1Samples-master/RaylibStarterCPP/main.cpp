﻿/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "string.h"
//#include "array"  
//#include "Player.h" //TODO:find time to use this

#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "raylib.h"
#include <vector>
#include <string>
#include <thread>

using namespace std;


int screenWidth = 1600;
int screenHeight = 900;

int winsNeeded = 3;
int state = 1; // 0 = none, 1 = menu, 2 = playing
int gameWon = 0; // 0 = no, 1 = yes, 2 = stalemate

int turn = 0;
int input = 0; //TODO: change to more useful name (purpose is camera angle key input)
float cameraDistance = 20;
float cameraAngle = 1.5708f; // set to 90* in radians
short gravity = 0;
short playerCount = 2;
int optionsPage = 0;
int resetMode = 0; //an option for games to clear immediately upon a player winning


//Constraints for options menu
const int maxPlayers = 8;
const int minPlayers = 1;

const int maxGravity = 3;
const int minGravity = 0;

const int minCheckSize = 1;

const int minWinsNeeded = 1;

const int maxOptions = 1;
const int minOptions = 0;

const int maxPlayerColor = 7;
const int minPlayerColor = 0;

const int maxPlayerAIType = 2;
const int minPlayerAIType = 0;

const int maxResetMode = 1;
const int minResetMode = 0;

Vector3 boardSize = { 3,3,3 }; //x,y,z, mapped to x,z,y for cube
int checkSize = 3; //will need to be reworked so that more check conditions can be established (mainly diagonals that exist when board is increased)

vector<vector<vector<short>>> arr = //The 3D vector of playerIDs that hold the current board state, use PlaceBlock() instead of adding directly
{
	{{0,0,0}, {0,0,0},{0,0,0}},
	{{0,0,0}, {0,0,0},{0,0,0}},
	{{0,0,0}, {0,0,0},{0,0,0}}
};
int selected[] = { boardSize.x,boardSize.z,boardSize.y };

const Color UIColors[] = { BROWN, DARKPURPLE, PURPLE, DARKBLUE, SKYBLUE, DARKGREEN, GREEN, RED, ORANGE, GRAY }; //Master list of colors for UI elements
const Color playerColorsList[] = { DARKBLUE, MAROON, GOLD, DARKGREEN, ORANGE, LIME, BEIGE, MAGENTA }; //Master list of colors to assing for players
int playerColors[] = { 0, 1, 2, 3, 4, 5, 6, 7 }; //The colorID assigned to each player
int playerAIType[] = { 0, 0, 0, 0, 0, 0, 0, 0 }; //The AI type of each player, 0 no AI, 1 Defence pref, 2 Attack pref

int playerTotalWins[] = { 0, 0, 0, 0, 0, 0, 0, 0 }; //A count for how many wins each player has gained over all matches in a session
//TODO: Switch to class based player system

std::thread aiThread; //thread used for base AI
int threadTurnCount = 0; //Turn count at which the thread occured

float BoardSizeMagnitude()
{
	return sqrt(pow(boardSize.x, 2) + pow(boardSize.y, 2) + pow(boardSize.z, 2));
}
float BoardSizeAveraged()
{
	return (boardSize.x + boardSize.y + boardSize.z) / 3;
}
int SecondSmallestBoardAxis()
{
	if ((boardSize.x <= boardSize.y && boardSize.x >= boardSize.z) || (boardSize.x >= boardSize.y && boardSize.x <= boardSize.z))
		return boardSize.x;
	if ((boardSize.y <= boardSize.x && boardSize.y >= boardSize.z) || (boardSize.y >= boardSize.x && boardSize.y <= boardSize.z))
		return boardSize.y;
	if ((boardSize.z <= boardSize.y && boardSize.z >= boardSize.x) || (boardSize.z >= boardSize.y && boardSize.z <= boardSize.x))
		return boardSize.z;

	return 0;
}
int LargestBoardAxis()
{
	int result = boardSize.x;

	if (result < boardSize.y)
		result = boardSize.y;
	if (result < boardSize.z)
		result = boardSize.z;

	return result;
}

void DrawCubeWires(Vector3 position, Vector3 size, Color color, int thickness, float gap) {
	for (int c = 0; c < thickness * thickness * thickness; c++) {
		float x = (c % thickness);
		x = (x * gap) - gap * ((thickness - 1) / 2);

		float y = ((c / thickness) % thickness);
		y = (y * gap) - gap * ((thickness - 1) / 2);

		float z = (c / (thickness * thickness));
		z = (z * gap) - gap * ((thickness - 1) / 2);

		DrawCubeWires({ x + position.x, y + position.y, z + position.z }, size.x, size.y, size.z, color);
	}
}

void Generate3DBoard()
{
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			for (int k = 0; k < boardSize.y; k++)
			{
				Vector3	tempPos = { i * 2 - boardSize.x + 1, (k * 2) + 1, j * 2 - boardSize.z + 1 };
				if (arr[i][j][k] == 0)
				{
					if (selected[0] == i && selected[1] == j && selected[2] == k)
						DrawCube(tempPos, 2, 2, 2, GRAY);
				}
				else
					DrawCube(tempPos, 2, 2, 2, playerColorsList[playerColors[arr[i][j][k] - 1]]);

				DrawCubeWires(tempPos, { 2.0f, 2.0f, 2.0f }, UIColors[k], 3, 0.01f);
			}
		}
	}
}
void Generate2DBoard()
{
	int scale = (20 * screenHeight) / ((21 * pow(BoardSizeAveraged(), 2)) + 20);
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			for (int k = 0; k < boardSize.y; k++)
			{
				//if (gravity != 3 ||(gravity == 3 && k == boardSize - 1)){
				Rectangle rec = { screenWidth - scale * (boardSize.x + 1) + i * scale, screenHeight + (j * scale) - ((k + 1) * (scale * 1.05) * boardSize.z) , scale, scale };

				if (arr[i][j][k] == 0)
				{
					DrawRectangleRec(rec, LIGHTGRAY);
					if (selected[0] == i && selected[1] == j && selected[2] == k)
					{
						DrawCircle(screenWidth - scale * (boardSize.x + 1) + i * scale + scale / 2, screenHeight + (j * scale) - ((k + 1) * (scale * 1.05) * boardSize.z) + scale / 2, scale / 2.5, playerColorsList[playerColors[turn % playerCount]]);
					}
				}
				else
					DrawRectangleRec(rec, playerColorsList[playerColors[arr[i][j][k] - 1]]);
				const char* text = "?";
				DrawRectangleLinesEx(rec, 2.0f, UIColors[k]);


				if (scale > 20)
				{
					DrawText(TextFormat("%i:%i", k, (i + 1) + j * (int)boardSize.x), rec.x + rec.width / 2 - MeasureText(TextFormat("%i:%i", k, (i + 1) + j * (int)boardSize.x), scale / 2) / 2, rec.y + scale / 2, scale / 2, BLACK);
				}
			}
			//}
		}
	}
}
void Check2DBoard()
{
	int scale = (20 * screenHeight) / ((21 * pow(BoardSizeAveraged(), 2)) + 20);
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			for (int k = 0; k < boardSize.y; k++)
			{
				Rectangle rec = { screenWidth - scale * (boardSize.x + 1) + i * scale, screenHeight + (j * scale) - ((k + 1) * (scale * 1.05) * boardSize.z) , scale, scale };
				if (CheckCollisionPointRec(GetMousePosition(), rec))
				{
					selected[0] = i;
					selected[1] = j;
					selected[2] = k;
					return;
				}
			}
		}
	}
	selected[0] = boardSize.x;
	selected[1] = boardSize.z;
	selected[2] = boardSize.y;
}

int CheckUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (checkArray[i][j][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckForward(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check back to forward rows
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (checkArray[i][k][j] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckRight(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check left to right rows
	int sum = 0;
	for (int i = 0; i < boardSize.z; i++)
	{
		for (int j = 0; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (checkArray[k][i][j] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}

int CheckForwardUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++) //starts from bottom plate
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j - k >= 0 && checkArray[i][j - k][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.x; i++)//starts from back wall
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column (starts at one to avoid double checking from first loop)
			{
				if ((boardSize.z - 1) - k >= 0 && j + k < boardSize.y && checkArray[i][(boardSize.z - 1) - k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckBackUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.z && checkArray[i][j + k][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.x; i++)//starts from back wall
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column(starts at one to avoid double checking from first loop)
			{
				if (j + k < boardSize.y && checkArray[i][k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckRightUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (i + k < boardSize.x && checkArray[i + k][j][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.z; i++)//starts from back wall
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column (starts at one to avoid double checking from first loop)
			{
				if (j + k < boardSize.y && checkArray[k][i][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckLeftUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (i - k >= 0 && checkArray[i - k][j][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.z; i++)//starts from back wall
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column (starts at one to avoid double checking from first loop)
			{
				if ((boardSize.x - 1) - k >= 0 && j + k < boardSize.y && checkArray[(boardSize.x - 1) - k][i][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}

int CheckForwardRight(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (i - k >= 0 && checkArray[i - k][k][j] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 1; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (i + k < boardSize.x && (boardSize.z - 1) - k >= 0 && checkArray[i + k][(boardSize.z - 1) - k][j] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckForwardLeft(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 1; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (i + k < boardSize.x && checkArray[i + k][k][j] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (i - k >= 0 && (boardSize.z - 1) - k >= 0 && checkArray[i - k][(boardSize.z - 1) - k][j] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}

int CheckForwardRightUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all 3D diagonal from back left bottom to forward right top
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++) //bottom panel
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j - k >= 0 && i + k < boardSize.x && checkArray[i + k][j - k][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 1; i < boardSize.x; i++) //back panel
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i + k < boardSize.x && (boardSize.z - 1) - k >= 0 && checkArray[i + k][(boardSize.z - 1) - k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.z; i++) //left panel
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i - k >= 0 && checkArray[k][i - k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckForwardLeftUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all 3D diagonal from back right bottom to forward left top
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++) //bottom panel
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j - k >= 0 && i - k >= 0 && checkArray[i - k][j - k][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.x; i++) //back panel
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i - k >= 0 && (boardSize.z - 1) - k >= 0 && checkArray[i - k][(boardSize.z - 1) - k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.z - 1; i++) //right panel
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i - k >= 0 && (boardSize.x - 1) - k >= 0 && checkArray[(boardSize.x - 1) - k][i - k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckBackRightUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.z && i + k < boardSize.x && checkArray[i + k][j + k][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i + k < boardSize.x && checkArray[i + k][k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 1; i < boardSize.z; i++)
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i + k < boardSize.z && checkArray[k][i + k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}
int CheckBackLeftUp(int checkID, vector<vector<vector<short>>> checkArray = arr) { //will check all verticle columns
	int sum = 0;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.y; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.z && i - k >= 0 && checkArray[i - k][j + k][k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.z; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i - k >= 0 && checkArray[i - k][k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	for (int i = 1; i < boardSize.z; i++)
	{
		for (int j = 1; j < boardSize.y; j++)
		{
			int counter = 0;
			for (int k = 0; k < boardSize.x; k++) //determine how many unbroken lines exist in this vertical column
			{
				if (j + k < boardSize.y && i + k < boardSize.z && (boardSize.x - 1) - k >= 0 && checkArray[(boardSize.x - 1) - k][i + k][j + k] == checkID) //increase if the line is unbroken
					counter++;
				else
					counter = 0;
				if (counter == checkSize) //if the unbroken line is long enough, add a win and reset the counter
				{
					counter = 0;
					sum++;
				}
			}
		}
	}
	return sum;
}

int CheckWins(int Check, vector<vector<vector<short>>> checkArray) {
	int sum = 0;

	sum += CheckUp(Check, checkArray);
	sum += CheckForward(Check, checkArray);
	sum += CheckRight(Check, checkArray);

	sum += CheckForwardUp(Check, checkArray);
	sum += CheckBackUp(Check, checkArray);
	sum += CheckRightUp(Check, checkArray);
	sum += CheckLeftUp(Check, checkArray);

	sum += CheckForwardRight(Check, checkArray);
	sum += CheckForwardLeft(Check, checkArray);

	sum += CheckForwardRightUp(Check, checkArray);
	sum += CheckForwardLeftUp(Check, checkArray);
	sum += CheckBackRightUp(Check, checkArray);
	sum += CheckBackLeftUp(Check, checkArray);


	return sum;
}
int CheckWins(int Check) {
	int sum = 0;

	sum += CheckUp(Check);
	sum += CheckForward(Check);
	sum += CheckRight(Check);

	sum += CheckForwardUp(Check);
	sum += CheckBackUp(Check);
	sum += CheckRightUp(Check);
	sum += CheckLeftUp(Check);

	sum += CheckForwardRight(Check);
	sum += CheckForwardLeft(Check);

	sum += CheckForwardRightUp(Check);
	sum += CheckForwardLeftUp(Check);
	sum += CheckBackRightUp(Check);
	sum += CheckBackLeftUp(Check);


	return sum;
}
int CheckWins(vector<vector<vector<short>>> checkArray) {
	int sum = 0;
	for (int Check = 1; Check < playerCount + 1; Check++)
	{
		sum = CheckWins(Check, checkArray);

		if (sum >= winsNeeded)
			break;
	}
	return sum;
}
int CheckWins() {
	int sum = 0;
	for (int Check = 1; Check < playerCount + 1; Check++)
	{
		sum = CheckWins(Check);

		if (sum >= winsNeeded)
			break;
	}
	return sum;
}

void CleanBoard() {

	if (aiThread.joinable())
		aiThread.join();

	arr.resize(boardSize.x);
	for (int i = 0; i < boardSize.x; i++)
	{
		arr[i].resize(boardSize.z);
		for (int j = 0; j < boardSize.z; j++)
		{
			arr[i][j].resize(boardSize.y);
			for (int k = 0; k < boardSize.y; k++)
			{
				arr[i][j][k] = 0;
			}
		}
	}
	turn = 0;
	threadTurnCount = turn;
	if (cameraDistance < BoardSizeMagnitude() * 4) //if camera is now too close, bring it to the closest point
		cameraDistance = BoardSizeMagnitude() * 4;

	gameWon = false;
}
void DeclareWin() {

	if (gameWon == 0)
	{
		//check if game has been won for the first time
		if (CheckWins() >= winsNeeded)
		{
			gameWon = 1;
			playerTotalWins[((turn - 1) % playerCount)]++;
		}
		//else, check if a stalemate has occured
		else
		{
			//loop through every box to identify if every possible move was made
			bool stalemate = true;
			for (int X = 0; X < boardSize.x; X++)
			{
				for (int Y = 0; Y < boardSize.z; Y++)
				{
					for (int Z = 0; Z < boardSize.y; Z++)
					{
						if (arr[X][Y][Z] == 0) //if blank box exists, stalemate has not yet occured
							stalemate = false;
					}
				}
			}
			if (stalemate == true)
			{
				gameWon = 2;
			}
		}
	}
}

void ConstrainCheckSize() {
	if (checkSize > SecondSmallestBoardAxis())
		checkSize = SecondSmallestBoardAxis();
}
void ConstrainWins() {
	if (winsNeeded > LargestBoardAxis() * 2)
		winsNeeded = LargestBoardAxis() * 2;
}

//TODO: determin if gravity should be changed to have immediate effects, prevents not allowing blocks placed under
bool GravityBoard(int X, int Y, int Z, vector<vector<vector<short>>>& checkArray) {
	bool noChanges = true;
	for (int i = 0; i < boardSize.x; i++)
	{
		for (int j = 0; j < boardSize.z; j++)
		{
			for (int k = 0; k < boardSize.y; k++)
			{
				if (k - 1 >= 0 && checkArray[i][j][k - 1] == 0 && checkArray[i][j][k] != 0 && !(i == X && j == Y && k - 1 == Z))
				{
					checkArray[i][j][k - 1] = checkArray[i][j][k];
					checkArray[i][j][k] = 0;
					noChanges = false;
				}
			}
		}
	}
	if (gravity == 3 && noChanges == false) //If full gravity is enabled and
		noChanges = GravityBoard(X, Y, Z, checkArray);
	return noChanges;
}

bool LegalMove(int PlayerID, int X, int Y, int Z, vector<vector<vector<short>>> checkArray)
{
	return /*Ensure move will not occur after win*/ (CheckWins(checkArray) < winsNeeded) &&/*Ensure move exists in bounds*/(X < boardSize.x&& Y < boardSize.z&& Z < boardSize.y) && (X > -1 && Y > -1 && Z > -1) &&/*Ensure move will not be inside an existing block*/(checkArray[X][Y][Z] == 0) /*Ensure move will not be placed under falling block*/ /* !((gravity == 1 || (gravity == 2 && PlayerID == 0)) && Z + 1 < boardSize.y  && checkArray[X][Y][Z + 1] != 0) */;
}
bool PlaceBlock(int PlayerID, int X, int Y, int Z, vector<vector<vector<short>>>& checkArray)
{
	if (LegalMove(PlayerID, X, Y, Z, checkArray))
	{
		if (gravity == 1 || (gravity == 2 && PlayerID == 0)) //Simulate gravity before placing piece
			GravityBoard(X, Y, Z, checkArray);

		checkArray[X][Y][Z] = PlayerID + 1;
		turn++;

		if (gravity == 3) //Simulate Full gravity after a piece is placed (forces the move to the lowest point before checks are made)
			GravityBoard(X, Y, Z, checkArray);

		DeclareWin();
		return true;
	}
	else
		return false;
}


void RandomMove(int PlayerID) {
	if (CheckWins() < winsNeeded)
		PlaceBlock(PlayerID, (rand() % (int)(boardSize.x)), (rand() % (int)(boardSize.z)), (rand() % (int)(boardSize.y)), arr); //Keep running until it the function returns true, meaning a valid move was made
}

void DefensiveAI(int PlayerID)
{
	vector<vector<vector<short>>> checkArray = arr;

	int currentEnemyPoints = CheckWins(((turn + 1) % playerCount) + 1, arr);
	int currentPoints = CheckWins(PlayerID + 1, arr);

	int aggressiveCheck = 0;
	int aggressiveX = -1;
	int agressiveY = -1;
	int agressiveZ = -1;

	int defenceCheck = 0;
	int defenceX = -1;
	int defenceY = -1;
	int defenceZ = -1;
	for (int X = 0; X < boardSize.x; X++)
	{
		for (int Y = 0; Y < boardSize.z; Y++)
		{
			for (int Z = 0; Z < boardSize.y; Z++)
			{
				if (LegalMove(PlayerID, X, Y, Z, checkArray))
				{
					//Detect which move the player after it would benifit the most from to block it from happening
					checkArray[X][Y][Z] = ((turn + 1) % playerCount) + 1;
					int tempCheck = CheckWins(((turn + 1) % playerCount) + 1, checkArray);

					if (defenceCheck < tempCheck)
					{
						defenceCheck = tempCheck;
						defenceX = X;
						defenceY = Y;
						defenceZ = Z;
					}

					checkArray[X][Y][Z] = PlayerID + 1;
					tempCheck = CheckWins(PlayerID + 1, checkArray);

					if (aggressiveCheck < tempCheck)
					{
						aggressiveCheck = tempCheck;
						aggressiveX = X;
						agressiveY = Y;
						agressiveZ = Z;
					}

					checkArray[X][Y][Z] = 0;
				}
			}
		}
	}
	if (defenceCheck > currentEnemyPoints && defenceX != -1 && defenceY != -1 && defenceZ != -1)
		PlaceBlock(PlayerID, defenceX, defenceY, defenceZ, arr);
	else if (aggressiveCheck > currentPoints && aggressiveX != -1 && agressiveY != -1 && agressiveZ != -1)
		PlaceBlock(PlayerID, aggressiveX, agressiveY, agressiveZ, arr);
	else
	{
		if (LegalMove(PlayerID, boardSize.x / 2, boardSize.z / 2, boardSize.y / 2, arr)) //Attempt to capitalize on the center
			PlaceBlock(PlayerID, boardSize.x / 2, boardSize.z / 2, boardSize.y / 2, arr);
		else
		{
			int tempTurn = turn;
			while (tempTurn == turn)
				RandomMove(PlayerID); //If no valid move can be found, attempt a random move
		}
	}
} //Will attempt to prioritize defensive moves, blocking the player, then will use aggressive moves if there are no others

void AggressiveAI(int PlayerID)
{
	vector<vector<vector<short>>> checkArray = arr;

	int currentEnemyPoints = CheckWins(((turn + 1) % playerCount) + 1, arr);
	int currentPoints = CheckWins(PlayerID + 1, arr);

	int aggressiveCheck = 0;
	int aggressiveX = -1;
	int agressiveY = -1;
	int agressiveZ = -1;

	int defenceCheck = 0;
	int defenceX = -1;
	int defenceY = -1;
	int defenceZ = -1;
	for (int X = 0; X < boardSize.x; X++)
	{
		for (int Y = 0; Y < boardSize.z; Y++)
		{
			for (int Z = 0; Z < boardSize.y; Z++)
			{
				if (LegalMove(PlayerID, X, Y, Z, checkArray))
				{
					//Detect which move the player after it would benifit the most from to block it from happening
					checkArray[X][Y][Z] = ((turn + 1) % playerCount) + 1;
					int tempCheck = CheckWins(((turn + 1) % playerCount) + 1, checkArray);

					if (defenceCheck < tempCheck)
					{
						defenceCheck = tempCheck;
						defenceX = X;
						defenceY = Y;
						defenceZ = Z;
					}

					checkArray[X][Y][Z] = PlayerID + 1;
					tempCheck = CheckWins(PlayerID + 1, checkArray);

					if (aggressiveCheck < tempCheck)
					{
						aggressiveCheck = tempCheck;
						aggressiveX = X;
						agressiveY = Y;
						agressiveZ = Z;
					}

					checkArray[X][Y][Z] = 0;
				}
			}
		}
	}
	if (aggressiveCheck > currentPoints && aggressiveX != -1 && agressiveY != -1 && agressiveZ != -1)
		PlaceBlock(PlayerID, aggressiveX, agressiveY, agressiveZ, arr);
	else if (defenceCheck > currentEnemyPoints && defenceX != -1 && defenceY != -1 && defenceZ != -1)
		PlaceBlock(PlayerID, defenceX, defenceY, defenceZ, arr);
	else
	{
		if (arr[boardSize.x / 2][boardSize.z / 2][boardSize.y / 2] == 0 && LegalMove(PlayerID, boardSize.x / 2, boardSize.z / 2, boardSize.y / 2, checkArray)) //Attempt to capitalize on the center
			PlaceBlock(PlayerID, boardSize.x / 2, boardSize.z / 2, boardSize.y / 2, arr);
		else
		{
			int tempTurn = turn;
			while (tempTurn == turn)
				RandomMove(PlayerID); //If no valid move can be found, attempt a random move
		}
	}
} //Will attempt to prioritize aggressive moves, gaining more points, then will use defensive moves if there are no others

int main(int argc, char* argv[])
{
	// Initialization
	//--------------------------------------------------------------------------------------
	SetConfigFlags(FLAG_WINDOW_RESIZABLE);
	InitWindow(screenWidth, screenHeight, "Tic Tac Toe Cubed");
	/*Camera camera{
		Vector3{ 4.0f, 2.0f, 4.0f },
		Vector3{ 0.0f, 1.8f, 0.0f },
		Vector3{ 0.0f, 1.0f, 0.0f },
		60.0f,
		CAMERA_PERSPECTIVE};*/
	Camera3D camera = { 0 };
	camera.position = Vector3{ 15.0f, 10.0f, 15.0f };
	camera.target = Vector3{ 0.0f, 0.0f, 0.0f };
	camera.up = Vector3{ 0.0f, 1.0f, 0.0f };
	camera.fovy = 45;
	camera.projection = CAMERA_PERSPECTIVE;

	Vector3 cubePos = Vector3{ 0.0f, 0.0f, 0.0f };

	SetTargetFPS(60);
	SetExitKey(KEY_TAB);

	CleanBoard();
	//--------------------------------------------------------------------------------------

	// Main game loop
	while (!WindowShouldClose() && state != 0)    // Detect window close button or ESC key
	{
		// Update
		//----------------------------------------------------------------------------------

		//Check if the screen has been resized
		if (IsWindowResized())
		{
			screenWidth = GetScreenWidth();
			screenHeight = GetScreenHeight();
		}

		if (state == 2)// only run these during runtime
		{
			//Handle input for camera (should increase at a radian a second)
			if (IsKeyDown(KEY_A) || IsKeyDown(KEY_LEFT))
				input++;
			if (IsKeyDown(KEY_D) || IsKeyDown(KEY_RIGHT))
				input--;
			cameraAngle += input * GetFrameTime(); //run input through delta time calculation, use it before changing use for camera distance
			input = 0; //reset input for zoom input

			if ((IsKeyDown(KEY_W) || IsKeyDown(KEY_UP)) && cameraDistance > BoardSizeMagnitude() * 4)
				input -= 25;
			if ((IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN)) && cameraDistance < 100)
				input += 25;
			cameraDistance += input * GetFrameTime(); //allows usage of delta time without declaring new variable
			input = 0; //reset input for next frame

			// Move camera based on input
			camera.target = Vector3{ 0.0f, BoardSizeMagnitude() / 2, 0.0f };
			camera.position.x = cameraDistance * (cos(cameraAngle));
			camera.position.z = cameraDistance * (sin(cameraAngle));
			camera.position.y = cameraDistance * 0.25 + BoardSizeMagnitude() / 2;

			UpdateCamera(&camera, CAMERA_CUSTOM);

			//Check all boards for mouse position - if the game is not won
			if (gameWon == 0)
			{
				Check2DBoard();
			}

			if (gameWon == 0) //ensure the game is still in play, saves on performace after the game is concluded
			{
				switch (playerAIType[(turn % playerCount)])//Determine what type of turn it is
				{
				case 0:
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && (CheckWins() < winsNeeded))
					{	//Check if selected move is not out of bounds

						PlaceBlock(turn % playerCount, selected[0], selected[1], selected[2], arr);

					}
					break;
				case 1:
					if (!aiThread.joinable())
					{
						aiThread = std::thread(DefensiveAI, turn % playerCount);
						threadTurnCount = turn;
					}
					else
					{
						if (turn > threadTurnCount)
							aiThread.join();
					}
					break;
				case 2:
					if (!aiThread.joinable())
					{
						aiThread = std::thread(AggressiveAI, turn % playerCount);
						threadTurnCount = turn;
					}
					else
					{
						if (turn > threadTurnCount)aiThread.join();
					}
					break;
				}
			}
			else
			{
				if (resetMode == 1)
				{
					CleanBoard();
				}
			}

			if (IsKeyDown(KEY_R))
			{
				CleanBoard();
			}
		}
		if (IsKeyDown(KEY_ESCAPE))
		{
			state = 1;
		}
		//----------------------------------------------------------------------------------

		// Draw
		//----------------------------------------------------------------------------------
		BeginDrawing();

		ClearBackground(RAYWHITE);

		if (state == 2) //Normal run state
		{
			BeginMode3D(camera);

			//Draw each 3D board
			Generate3DBoard();
			//GenerateFlat3DBoard(BoardOne, 0);
			//GenerateFlat3DBoard(BoardTwo, 1);
			//GenerateFlat3DBoard(BoardThree, 2);

			DrawLine3D({ -2,0,-boardSize.z }, { 0,0,-5 - boardSize.z }, RED);
			DrawLine3D({ 2,0,-boardSize.z }, { 0,0,-5 - boardSize.z }, RED);
			//DrawGrid(1000, 5.0f);

			EndMode3D();

			//Draw flat boards for UI
			Generate2DBoard();

			//DrawTextGUI
			DrawFPS(10, 10);
			int yPos = 30;
			for (int i = 0; i < playerCount; i++)
			{
				float denominator = ((float)playerTotalWins[0] + (float)playerTotalWins[1] + (float)playerTotalWins[2] + (float)playerTotalWins[3] + (float)playerTotalWins[4] + (float)playerTotalWins[5] + (float)playerTotalWins[6] + (float)playerTotalWins[7]);
				if (denominator < 1)
					denominator = 1;
				int percentage = round((playerTotalWins[i] / denominator) * 100);
				DrawText(TextFormat("Player %i: %i - Total wins: %i", i + 1, CheckWins(i + 1), playerTotalWins[i]), 10, yPos, 50, DARKGRAY);
				yPos += 60;
			}
			DrawText(TextFormat("Turn: %i", turn), 10, yPos, 30, DARKGRAY);

			//Check if win text needs to be displayed
			if (gameWon == 1)
				DrawText(TextFormat("Winner: Player %i", ((turn - 1) % playerCount) + 1), (screenWidth * 0.75) / 2, (screenHeight * 1.50) / 2, 50, DARKGRAY);
			else if (gameWon == 2)
				DrawText("Stalemate", (screenWidth * 0.75) / 2, (screenHeight * 1.50) / 2, 50, DARKGRAY);
		}
		else if (state == 1) //Main menu state
		{
			DrawText("TicTacToe", (screenWidth * 0.82) / 2, (screenHeight * 0.20) / 2, 50, DARKGRAY);
			DrawText("3", (screenWidth * 0.82) / 2 + 280, (screenHeight * 0.20) / 2 - 10, 35, DARKGRAY);
			for (int i = 0; i < 4; i++)
			{
				Rectangle menuItem = { (screenWidth * 0.75) / 2, (screenHeight * (0.40 + (i * 0.40))) / 2, 410, 100 };
				const char* text = "N/A";
				switch (i)
				{
				case 0:
					menuItem.width = 300;
					text = "Play";
					break;
				case 1:
					menuItem.width = 450;
					text = "Continue";
					break;
				case 2:
					menuItem.width = 420;
					text = "Options";
					break;
				case 3:
					menuItem.width = 300;
					text = "Exit";
					break;
				}
				menuItem.x = (screenWidth / 2) - (menuItem.width / 2);

				if (CheckCollisionPointRec(GetMousePosition(), menuItem))
				{
					DrawRectangleRec(menuItem, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
					{
						switch (i)
						{
						case 0: //Creat new game
							state = 2;
							CleanBoard();
							break;
						case 1: //Resume game
							state = 2;
							if (arr.size() != boardSize.x || arr[0].size() != boardSize.z || arr[0][0].size() != boardSize.y)
								CleanBoard();
							break;
						case 2: //Options
							state = 3;
							break;
						case 3: //Exit
							state = 0;
							break;
						default:
							break;
						}
					}
				}
				else
					DrawRectangleRec(menuItem, DARKGRAY);
				DrawText(text, menuItem.x + ((menuItem.width - MeasureText(text, 90)) / 2), menuItem.y + 6, 90, WHITE);
			}
		}
		else if (state == 3) //options state
		{
			//intialize variables for options such as buttons and info boxes, these variables will constantly change
			const char* text = "N/A";
			string textString = "N/A"; //used when appending int and text

			const char* infoFirstText = "";
			const char* infoSecondText = "";
			Rectangle optionBox = { 0, 0, 0, 0 };
			Rectangle infoBox = { 0, 0, 0, 0 };
			Rectangle infoInnerBox = { 0, 0, 0, 0 };

			float gap = 0.30; //Constant for how far away the boxes should be (may be replaced per page)
			int incrementer = 1; // used in conjunction with gap to identify where it should be placed


			DrawText("Options", (screenWidth / 2) - ((float)100 / 2) - 150, (screenHeight * ((0.5 * 0.30f) - 0.15f)) / 2, 50, DARKGRAY);

			switch (optionsPage)
			{
			case 0:
				gap = 0.30;

				//--Board size x option
				//Box
				optionBox = { (screenWidth / 2) - ((float)100 / 2) - 150, (screenHeight * ((incrementer++ * gap) - 0.15f)) / 2, 100, 100 };
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						boardSize.x++;
						if (boardSize.x > 10)
							boardSize.x = 2;

						ConstrainCheckSize();
						ConstrainWins();
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						boardSize.x--;
						if (boardSize.x < 2)
							boardSize.x = 10;

						ConstrainCheckSize();
						ConstrainWins();
					}
					infoFirstText = "Increases the X axis of the game (Range: 2-10)";
					infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				textString = to_string((int)boardSize.x);
				text = textString.c_str();
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);
				//draw extra times by symbol
				text = "x";
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2) + 75, optionBox.y + 6, 90, BLACK);


				//--Board size y option
				//Box
				optionBox = { (screenWidth / 2) - ((float)100 / 2), optionBox.y, 100, 100 };
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						boardSize.y++;
						if (boardSize.y > 10)
							boardSize.y = 2;

						ConstrainCheckSize();
						ConstrainWins();
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						boardSize.y--;
						if (boardSize.y < 2)
							boardSize.y = 10;

						ConstrainCheckSize();
						ConstrainWins();
					}
					infoFirstText = "Increases the Y axis of the game (Range: 2-10)";
					infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				textString = to_string((int)boardSize.y);
				text = textString.c_str();
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);
				//draw extra times by symbol
				text = "x";
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2) + 75, optionBox.y + 6, 90, BLACK);


				//--Board size z option
				//Box
				optionBox = { (screenWidth / 2) - ((float)100 / 2) + 150, optionBox.y, 100, 100 };
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						boardSize.z++;
						if (boardSize.z > 10)
							boardSize.z = 2;

						ConstrainCheckSize();
						ConstrainWins();
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						boardSize.z--;
						if (boardSize.z < 2)
							boardSize.z = 10;

						ConstrainCheckSize();
						ConstrainWins();
					}
					infoFirstText = "Increases the Z axis of the game (Range: 2-10)";
					infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				textString = to_string((int)boardSize.z);
				text = textString.c_str();
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);



				//--Gravity Options
				//Box
				optionBox = { (screenWidth / 2) - ((float)550 / 2), (screenHeight * ((incrementer++ * gap) - 0.15f)) / 2, 550, 100 };
				switch (gravity)
				{
				case 0:
					optionBox.x = (screenWidth / 2) - ((float)550 / 2);
					optionBox.width = 550;
					text = "No Gravity";
					break;
				case 1:
					optionBox.x = (screenWidth / 2) - ((float)650 / 2);
					optionBox.width = 650;
					text = "Turn-Gravity";
					break;
				case 2:
					optionBox.x = (screenWidth / 2) - ((float)700 / 2);
					optionBox.width = 700;
					text = "Round-Gravity";
					break;
				case 3:
					optionBox.x = (screenWidth / 2) - ((float)600 / 2);
					optionBox.width = 600;
					text = "Full-Gravity";
					break;
				default:
					text = "Error";
					break;
				}
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						gravity++;
						if (gravity > maxGravity)
							gravity = minGravity;
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						gravity--;
						if (gravity < minGravity)
							gravity = maxGravity;
					}

					switch (gravity) //duplicate switch case purely for info box TODO: reduce? figure out a way to include both cleanly
					{
					case 0:
						infoFirstText = "Blocks will stay frozen in the";
						infoSecondText = "position they are placed";
						infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 85 };
						break;
					case 1:
						infoFirstText = "Blocks will fall down one position lower";
						infoSecondText = "directly before the next round is started";
						infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoSecondText, 35) + 20, 85 };
						break;
					case 2:
						infoFirstText = "Blocks will fall down one position lower";
						infoSecondText = "directly before a move is made";
						infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 85 };
						break;
					case 3:
						infoFirstText = "Blocks will fall down to the lowest point,";
						infoSecondText = "this happens as soon as a block is placed";
						infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoSecondText, 35) + 20, 85 };
						break;
					default:
						infoFirstText = "Error";
						infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
						break;
					}
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);
				//indicator
				for (int i = 0; i < maxGravity - minGravity + 1; i++)
				{
					Color circleColor = WHITE;
					if (i == gravity)
						circleColor = BLACK;
					DrawCircle(optionBox.x + (optionBox.width / 4) + (((float)(i + 1) / (maxGravity - minGravity + 2)) * (optionBox.width / 2)), optionBox.y + optionBox.height - 10, 5, circleColor);
				}



				//--checkSize option
				//Box
				optionBox = { (screenWidth / 2) - ((float)500 / 2), (screenHeight * ((incrementer++ * gap) - 0.15f)) / 2, 500, 100 };
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						checkSize++;
						if (checkSize > SecondSmallestBoardAxis())
							checkSize = minCheckSize;
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						checkSize--;
						if (checkSize < minCheckSize)
							checkSize = SecondSmallestBoardAxis();

						ConstrainCheckSize();
					}
					infoFirstText = "How many in a row needed for a point";
					infoSecondText = "(smaller or equal to second smallest axis)";
					infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoSecondText, 35) + 20, 85 };
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				textString = to_string(checkSize) + " in a row";
				text = textString.c_str();
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);
				//indicator
				for (int i = 0; i < SecondSmallestBoardAxis() - minCheckSize + 1; i++)
				{
					Color circleColor = WHITE;
					if (i == checkSize - 1)
						circleColor = BLACK;
					DrawCircle(optionBox.x + (optionBox.width / 4) + (((float)(i + 1) / (SecondSmallestBoardAxis() - minCheckSize + 2)) * (optionBox.width / 2)), optionBox.y + optionBox.height - 10, 5, circleColor);
				}



				//--win number option
				//Box
				optionBox = { (screenWidth / 2) - ((float)675 / 2), (screenHeight * ((incrementer++ * gap) - 0.15f)) / 2, 675, 100 };
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						winsNeeded++;
						if (winsNeeded > LargestBoardAxis() * 2)
							winsNeeded = minWinsNeeded;
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						winsNeeded--;
						if (winsNeeded < minWinsNeeded)
							winsNeeded = LargestBoardAxis() * 2;
					}
					infoFirstText = "How many points/lines needed to win";
					infoSecondText = "(smaller or equal to twice the largest axis)";
					infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoSecondText, 35) + 20, 85 };
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				textString = to_string(winsNeeded) + " point to win";
				text = textString.c_str();
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);
				//indicator
				for (int i = 0; i < LargestBoardAxis() * 2 - minWinsNeeded + 1; i++)
				{
					Color circleColor = WHITE;
					if (i == winsNeeded - 1)
						circleColor = BLACK;
					DrawCircle(optionBox.x + (optionBox.width / 4) + (((float)(i + 1) / ((LargestBoardAxis() * 2) - minWinsNeeded + 2)) * (optionBox.width / 2)), optionBox.y + optionBox.height - 10, 5, circleColor);
				}



				//--reset mode option
				//Box
				optionBox = { (screenWidth / 2) - ((float)675 / 2), (screenHeight * ((incrementer++ * gap) - 0.15f)) / 2, 675, 100 };
				switch (resetMode)
				{
				case 0:
					optionBox.x = (screenWidth / 2) - ((float)725 / 2);
					optionBox.width = 725;
					text = "Wait for reset";
					break;
				case 1:
					optionBox.x = (screenWidth / 2) - ((float)825 / 2);
					optionBox.width = 825;
					text = "Reset immediately";
					break;
				default:
					text = "Error";
					break;
				}
				if (CheckCollisionPointRec(GetMousePosition(), optionBox))
				{
					DrawRectangleRec(optionBox, GRAY);
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
					{
						resetMode++;
						if (resetMode > maxResetMode)
							resetMode = minResetMode;
					}
					if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
					{
						resetMode--;
						if (resetMode < minResetMode)
							resetMode = maxResetMode;
					}
					infoFirstText = "If the game should immediately";
					infoSecondText = "clear the board once a player wins";
					infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoSecondText, 35) + 20, 85 };
					infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
				}
				else
					DrawRectangleRec(optionBox, DARKGRAY);
				//text
				DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);
				//indicator
				for (int i = 0; i < maxResetMode - minResetMode + 1; i++)
				{
					Color circleColor = WHITE;
					if (i == resetMode)
						circleColor = BLACK;
					DrawCircle(optionBox.x + (optionBox.width / 4) + (((float)(i + 1) / (maxResetMode - minResetMode + 2)) * (optionBox.width / 2)), optionBox.y + optionBox.height - 10, 5, circleColor);
				}

				break;
			case 1:// player list
				gap = 0.2;
				for (int i = minPlayers - 1; i < maxPlayers; i++)
				{
					if (i < playerCount)
					{
						//Player color box (appears beside player box)
						//box
						optionBox = { (screenWidth / 2) - ((float)470 / 2), (screenHeight * (((i)*gap) + 0.15f)) / 2, 75, 75 };
						if (CheckCollisionPointRec(GetMousePosition(), optionBox))
						{
							//Create ligh color of base for highlighting
							Color tempPlayerColor = playerColorsList[playerColors[i]];
							tempPlayerColor.a = playerColorsList[playerColors[i]].a - 50;
							DrawRectangleRec(optionBox, tempPlayerColor);
							if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
							{
								playerColors[i]++;
								if (playerColors[i] > maxPlayerColor)
									playerColors[i] = minPlayerColor;
							}
							if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
							{
								playerColors[i]--;
								if (playerColors[i] < minPlayerColor)
									playerColors[i] = maxPlayerColor;
							}
							infoFirstText = "Change color";
							infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
							infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
						}
						else
							DrawRectangleRec(optionBox, playerColorsList[playerColors[i]]);


						//Player count/AI selection
						//box
						optionBox = { optionBox.x + optionBox.width, optionBox.y, 320, 75 };
						if (CheckCollisionPointRec(GetMousePosition(), optionBox))
						{
							DrawRectangleRec(optionBox, GRAY);
							if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
							{
								playerAIType[i]++;
								if (playerAIType[i] > maxPlayerAIType)
									playerAIType[i] = minPlayerAIType;
								//Convert to AI/Player
							}
							if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
							{
								playerAIType[i]--;
								if (playerAIType[i] < minPlayerAIType)
									playerAIType[i] = maxPlayerAIType;
								//Convert to AI/Player
							}
							infoFirstText = "Convert between a player or AI slot";
							infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
							infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
						}
						else
							DrawRectangleRec(optionBox, DARKGRAY);
						//text
						switch (playerAIType[i])
						{
						case 0:// No AI used
							textString = "Player " + to_string(i + 1);
							break;
						case 1:
							textString = "Block " + to_string(i + 1);
							break;
						case 2:
							textString = "Attack " + to_string(i + 1);
							break;
						default:
							textString = "error";
							break;
						}
						text = textString.c_str();
						DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 65)) / 2), optionBox.y + 6, 65, WHITE);
					}
					if (i == playerCount - 1 && i != 0) //Remove last player (appears beside the final existing player)
					{
						//box
						optionBox = { optionBox.x + optionBox.width, optionBox.y, 75, 75 };
						if (CheckCollisionPointRec(GetMousePosition(), optionBox))
						{
							DrawRectangleRec(optionBox, GRAY);
							if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
							{
								playerCount--;
								if (playerCount < minPlayers)
									playerCount = minPlayers;
							}

							infoFirstText = "Remove Player";
							infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
							infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
						}
						else
							DrawRectangleRec(optionBox, DARKGRAY);
						//text
						text = "-";
						DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 65)) / 2), optionBox.y + 6, 65, WHITE);
					}
					if (i == playerCount) //Add new player (appears in the first empty player slot)
					{
						//box
						optionBox = { (screenWidth / 2) - ((float)470 / 2), (screenHeight * (((i)*gap) + 0.15f)) / 2, 75, 75 };
						if (CheckCollisionPointRec(GetMousePosition(), optionBox))
						{
							DrawRectangleRec(optionBox, GRAY);
							if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
							{
								playerCount++;
								if (playerCount > maxPlayers)
									playerCount = minPlayers;
							}
							infoFirstText = "Add Player";
							infoBox = { GetMousePosition().x, GetMousePosition().y, (float)MeasureText(infoFirstText, 35) + 20, 55 };
							infoInnerBox = { GetMousePosition().x + 5, GetMousePosition().y + 5, infoBox.width - 10, infoBox.height - 10 };
						}
						else
							DrawRectangleRec(optionBox, DARKGRAY);
						//text
						text = "+";
						DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 65)) / 2), optionBox.y + 6, 65, WHITE);
					}
				}
				break;
			}

			//--Right options page button
			//Box
			optionBox = { screenWidth - (float)100, ((float)screenHeight / 2) - ((float)100 / 2), 100, 100 };
			if (CheckCollisionPointRec(GetMousePosition(), optionBox))
			{
				DrawRectangleRec(optionBox, GRAY);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
				{
					optionsPage++;
					if (optionsPage > maxOptions)
						optionsPage = minOptions;
				}
				if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
				{
					optionsPage--;
					if (optionsPage < minOptions)
						optionsPage = maxOptions;
				}
			}
			else
				DrawRectangleRec(optionBox, DARKGRAY);
			//text
			text = ">";
			DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);



			//--Left options page button
			//Box
			optionBox = { 0, ((float)screenHeight / 2) - ((float)100 / 2), 100, 100 };
			if (CheckCollisionPointRec(GetMousePosition(), optionBox))
			{
				DrawRectangleRec(optionBox, GRAY);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
				{
					optionsPage--;
					if (optionsPage < minOptions)
						optionsPage = maxOptions;
				}
				if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
				{
					optionsPage++;
					if (optionsPage > maxOptions)
						optionsPage = minOptions;
				}
			}
			else
				DrawRectangleRec(optionBox, DARKGRAY);
			//text
			text = "<";
			DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);



			//--exit option
			//Box
			optionBox = { (screenWidth / 6) - ((float)310 / 2), (screenHeight * ((6 * 0.3f) - 0.15f)) / 2, 310, 100 };
			if (CheckCollisionPointRec(GetMousePosition(), optionBox))
			{
				DrawRectangleRec(optionBox, GRAY);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) //Left click options
				{
					state = 1;
				}
				if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)) //Right click options
				{
					//secret?
				}
			}
			else
				DrawRectangleRec(optionBox, DARKGRAY);
			//text
			text = "Back";
			DrawText(text, optionBox.x + ((optionBox.width - MeasureText(text, 90)) / 2), optionBox.y + 6, 90, WHITE);

			DrawRectangleRec(infoBox, BLACK);//Draw the info box
			DrawRectangleRec(infoInnerBox, WHITE);//Draw the info box
			DrawText(infoFirstText, infoInnerBox.x + ((infoInnerBox.width - MeasureText(infoFirstText, 35)) / 2), infoInnerBox.y + 6, 35, BLACK);
			DrawText(infoSecondText, infoInnerBox.x + ((infoInnerBox.width - MeasureText(infoSecondText, 35)) / 2), infoInnerBox.y + 35, 35, BLACK);
		}
		EndDrawing();
		//----------------------------------------------------------------------------------
	}

	// De-Initialization
	//--------------------------------------------------------------------------------------   
	CloseWindow();        // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}
